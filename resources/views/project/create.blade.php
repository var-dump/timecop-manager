@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Create</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/project') }}">
                            {!! csrf_field() !!}

                            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                <label for="title" class="col-md-4 control-label">Title</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="title" name="title"
                                           value="{{ old('title') }}">

                                    @if ($errors->has('title'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                <label for="description" class="col-md-4 control-label">Description</label>

                                <div class="col-md-6">
                                    <textarea class="form-control" id="description"
                                              name="description">{{ old('description') }}</textarea>

                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
                                <label for="user_id" class="col-md-4 control-label">Assign Member</label>

                                <div class="col-md-6">
                                    <select class="form-control" id="user_id" name="user_id">
                                        <option disabled selected>Select an user</option>

                                        @foreach($users as $user)
                                            <option value="{{ $user->id }}" @if(old('user_id') == $user->id) selected @endif>{{ $user->name }}</option>
                                        @endforeach
                                    </select>

                                    @if($errors->has('user_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('user_id') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('team_id') ? ' has-error' : '' }}">
                                <label for="team_id" class="col-md-4 control-label">Assign Team</label>

                                <div class="col-md-6">
                                    <select class="form-control" id="team_id" name="team_id">
                                        <option disabled selected>Select a team</option>

                                        @foreach($teams as $team)
                                            <option value="{{ $team->id }}" @if(old('team_id') == $team->id) selected @endif>{{ $team->name }}</option>
                                        @endforeach
                                    </select>

                                    @if($errors->has('team_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('team_id') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-btn fa-sign-in"></i>Create
                                    </button>
                                </div>
                            </div>
                        </form>
                        @if(session('status'))
                            <p class="text-success">{{ session('status') }}</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
