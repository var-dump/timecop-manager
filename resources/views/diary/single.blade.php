@extends('layouts.app')

@inject('Helper', 'App\Http\Helpers\Helper')

@section('content')
    <div class="container">
        @if(session('status'))
            <p class="text-danger">{{ session('status') }}</p>
        @endif

        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="col-md-6">
                    <a href="" class="btn btn-primary">Edit</a>
                </div>
                <div class="col-md-6">
                    <form action="{{ url('/diary/' . $diary->id) }}" method="post">
                        {!! method_field('DELETE') !!}
                        {!! csrf_field() !!}

                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </div>
                
                @if(count($diary->status))
                    <h3 class="page-title">{{ $diary->status }}</h3>
                @endif

                <div class="progress">
                    <div class="progress-bar progress-bar-success progress-bar-striped active"
                         role="progressbar"
                         aria-valuenow="{{ $Helper::activity($diary->keyboard, $diary->clicks, $diary->drags) }}"
                         aria-valuemin="0" aria-valuemax="100"
                         style="min-width: 2em; width: {{ $Helper::activity($diary->keyboard, $diary->clicks, $diary->drags) }}%;">
                        {{ $Helper::activity($diary->keyboard, $diary->clicks, $diary->drags) }}%
                    </div>
                </div>

                <div class="image">
                    @if(count($diary->webcamFileName))
                        <img src="{{ url('/diary/image/' . $diary->webcamFileName) }}"
                             alt="{{ $diary->webcamFileName }}" class="img-responsive ">
                    @endif
                    <img src="{{ url('/diary/image/' . $diary->screenshotFileName) }}"
                         alt="{{ $diary->screenshotFileName }}" class="img-responsive">
                </div>

                {{ $diary->created_at->toDayDateTimeString() }}

                <h4>{{ $diary->project }}</h4>
            </div>
        </div>
    </div>
@endsection
