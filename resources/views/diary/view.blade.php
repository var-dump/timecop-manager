@extends('layouts.app')

@inject('Helper', 'App\Http\Helpers\Helper')

@section('content')
    <div class="container">
        @if(session('status'))
            <p class="bg-primary">{{ session('status') }}</p>
        @endif

        <div class="row">
            {{--<div class="col-md-8 col-md-offset-2">--}}
            {{--<div class="panel panel-default">--}}
            {{--<div class="panel-heading">Select a project</div>--}}
            {{--<div class="panel-body">--}}
            {{--<form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">--}}
            {{--{!! csrf_field() !!}--}}

            {{--<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">--}}
            {{--<label for="projects" class="col-md-4 control-label">Projects</label>--}}

            {{--<div class="col-md-6">--}}
            {{--<select class="form-control" id="projects" name="user[]">--}}
            {{--<option selected disabled>All projects</option>--}}

            {{--@foreach($projects as $project)--}}
            {{--<option value="{{ $project->id }}">{{ $project->title }}</option>--}}
            {{--@endforeach--}}
            {{--</select>--}}

            {{--@if ($errors->has('email'))--}}
            {{--<span class="help-block">--}}
            {{--<strong>{{ $errors->first('email') }}</strong>--}}
            {{--</span>--}}
            {{--@endif--}}
            {{--</div>--}}
            {{--</div>--}}

            {{--<div class="form-group">--}}
            {{--<div class="col-md-6 col-md-offset-4">--}}
            {{--<button type="submit" class="btn btn-primary">--}}
            {{--<i class="fa fa-btn fa-sign-in"></i>Select--}}
            {{--</button>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</form>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}

            <div class="col-md-12">
                @foreach($diaries as $diary)
                    <div class="col-md-3">
                        <a href="{{ url('/diary/' . $diary->id) }}">
                            {{ $diary->status or '' }}
                            <img src="{{ url('/diary/image/' . $diary->screenshotFileName) }}"
                                 alt="{{ $diary->screenshotFileName }}" class="img-responsive">
                        </a>
                        <div class="row">
                            <div class="col-md-10">

                                {{ $diary->created_at->toDayDateTimeString() }}
                            </div>
                            <div class="col-md-2">

                                @if($diary->mood === 'online')
                                    <span class="glyphicon glyphicon-time online" aria-hidden="true"></span>
                                @elseif($diary->mood === 'offline')
                                    <span class="glyphicon glyphicon-off offline" aria-hidden="true"></span>
                                @else
                                    <span class="glyphicon glyphicon-exclamation-sign manual" aria-hidden="true"></span>
                                @endif
                            </div>
                        </div>

                        <div class="progress">
                            <div class="progress-bar progress-bar-success progress-bar-striped active"
                                 role="progressbar"
                                 aria-valuenow="{{ $Helper::activity($diary->keyboard, $diary->clicks, $diary->drags) }}"
                                 aria-valuemin="0" aria-valuemax="100"
                                 style="min-width: 2em; width: {{ $Helper::activity($diary->keyboard, $diary->clicks, $diary->drags) }}%;">
                                {{ $Helper::activity($diary->keyboard, $diary->clicks, $diary->drags) }}%
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            {!! $diaries->links() !!}
        </div>
    </div>
@endsection
