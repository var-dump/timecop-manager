@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Create user</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/createuser') }}">
                        {!! csrf_field() !!}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" id="name" name="name"
                                value="{{ old('name') }}">

                                
                            </div>
                        </div>

                        <div class="form-group">
                        <label for="email" class="col-md-4 control-label">Email</label>

                            <div class="col-md-6">
                                <input class="form-control" id="email" name="email" value="" type="text">

                            </div>
                        </div>

                        <div class="form-group">
                                <label for="passwrod" class="col-md-4 control-label">Password</label>

                                <div class="col-md-6">
                                    <input class="form-control" id="password" name="password" value="" type="password">

                                                                    </div>
                            </div>

                        <div class="form-group">
                            <label for="description" class="col-md-4 control-label">Skills</label>

                            <div class="col-md-6">
                                <textarea class="form-control" id="skills"
                                name="skills"></textarea>

                                
                            </div>
                        </div>

                        
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-sign-in"></i>Create
                                </button>
                            </div>
                        </div>
                    </form>
                    @if(session('status'))
                    <p class="text-success">{{ session('status') }}</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
