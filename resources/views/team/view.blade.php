@extends('layouts.app')

@section('content')
    <div class="container">
        @if(session('status'))
            <p class="text-danger">{{ session('status') }}</p>
        @endif

        <div class="row">
            @if(count($teams))
                @foreach($teams as $team)
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    <a href="{{ url("/team/$team->id") }}">{{ $team->name }}</a>

                                    <form action="{{ url("/team/$team->id") }}" method="post">
                                        {!! method_field('DELETE') !!}
                                        {!! csrf_field() !!}

                                        <button type="submit" class="close" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </form>

                                    <a href="{{ url("/team/$team->id/edit") }}" class="close" aria-label="Edit">
                                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                    </a>
                                </h3>
                            </div>
                            <div class="panel-body">
                                {{ $team->description }}
                            </div>
                            <div class="panel-footer">
                                @foreach($team->members as $member)
                                    {{ $member->name }}
                                @endforeach

                                @foreach($team->projects as $project)
                                    Projects: <a href="{{ url("/project/$project->id") }}">{{ $project->title }}</a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endforeach
            @else
                <p>No team is available. <a href="{{ url('/team/create') }}">Click here</a> to create one.</p>
            @endif
        </div>
    </div>
@endsection
