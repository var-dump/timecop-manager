<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diaries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->integer('second');
            $table->string('time');
            $table->integer('project_id')->unsigned()->index();
            $table->string('project');
            $table->integer('workId');
            $table->string('screenshotFileName');
            $table->string('webcamFileName');
            $table->integer('keyboard');
            $table->integer('drags');
            $table->integer('clicks');
            $table->string('status')->nullable();
            $table->string('ip');
            $table->string('localTime');
            $table->string('city');
            $table->string('country');
            $table->string('mood');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('diaries');
    }
}
