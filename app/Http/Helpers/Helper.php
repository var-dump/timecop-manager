<?php

namespace app\Http\Helpers;


class Helper
{
    public static function activity($keyboard, $clicks, $drags)
    {
        if (($keyboard + $clicks + $drags) / 3 >= 100) {

            return 100;
        } elseif (($keyboard + $clicks + $drags) / 3 >= 90) {

            return 90;
        } elseif (($keyboard + $clicks + $drags) / 3 >= 80) {

            return 80;
        } elseif (($keyboard + $clicks + $drags) / 3 >= 70) {

            return 70;
        } elseif (($keyboard + $clicks + $drags) / 3 >= 60) {

            return 60;
        } elseif (($keyboard + $clicks + $drags) / 3 >= 50) {

            return 50;
        } elseif (($keyboard + $clicks + $drags) / 3 >= 40) {

            return 40;
        } elseif (($keyboard + $clicks + $drags) / 3 >= 30) {

            return 30;
        } elseif (($keyboard + $clicks + $drags) / 3 >= 20) {

            return 20;
        } elseif (($keyboard + $clicks + $drags) / 3 >= 10) {

            return 10;
        }

        return 0;
    }
}