<?php

namespace App\Http\Controllers;

use App\Diary;
use App\Project;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use phpDocumentor\Reflection\DocBlock\DescriptionTest;
use Storage;

class DiaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('diary.view')->with(['projects' => Project::paginate(15), 'diaries' => Diary::paginate(15)]);

//        $diary = Diary::orderBy('id', 'desc')->get()->where('project', 'Title')->unique('workId')->values()->all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param Diary $diary
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function show(Diary $diary)
    {
        return view('diary.single')->with(['diary' => $diary]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Find the particular diary by ID
        $diary = Diary::all()->find($id);

        // If the particular $diary contain the webcamShot then everything will be deleted
        if (count($diary->webcamFileName)) {

            Storage::delete('diary/' . $diary->screenshotFileName, 'diary/' . $diary->webcamFileName);

            Diary::destroy($id);

            return redirect('/diary')->with('status', trans('Diary has been deleted successfully'));
        }

        // If webcamHost doesn't exist then only screenshot will be deleted
        Storage::delete('diary/' . $diary->screenshotFileName);

        Diary::destroy($id);

        return redirect('/diary')->with('status', trans('Diary has been deleted successfully'));
    }
}
