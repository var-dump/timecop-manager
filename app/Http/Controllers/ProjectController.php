<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProjectRequest;
use App\Project;
use App\Team;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('project.view')->with(['projects' => Project::all()]);

//        return Project::with('team')->find(1);
//        return Project::all()->first()->team->name;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('project.create')->with(['users' => User::all(), 'teams' => Team::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProjectRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectRequest $request)
    {
        Project::create($request->all());

        return back()->with('status', trans('Project has been created successfully'))->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param Project $project
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function show(Project $project)
    {
        return view('project.single', compact('project'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Project $project
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function edit(Project $project)
    {
        return view('project.edit')->with(['project' => $project, 'users' => User::all(), 'teams' => Team::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProjectRequest|Request $request
     * @param Project $project
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(ProjectRequest $request, Project $project)
    {
        if (!$request->has('user') && !$request->has('team')) {
            $modified_request = array_add($request->all(), 'user_id', 0);
            $modified_request = array_add($modified_request, 'team_id', 0);

            $project->update($modified_request);
        } elseif (!$request->has('user')) {
            $modified_request = array_add($request->all(), 'user_id', 0);

            $project->update($modified_request);
        } elseif (!$request->has('team')) {
            $modified_request = array_add($request->all(), 'team_id', 0);

            $project->update($modified_request);
        }

        $project->update($request->all());

        return back()->with('status', trans('Project has been updated successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Project::destroy($id);

        return back()->with('status', trans('Project has been deleted successfully.'));
    }
}
