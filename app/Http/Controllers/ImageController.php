<?php

namespace App\Http\Controllers;

use File;
use Storage;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ImageController extends Controller
{
    /**
     * Show an image from the Storage directory
     *
     * @param $image
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function show($image)
    {
        $path = storage_path() . '/app/diary/' . $image;

        if (file_exists($path)) {
            $type = File::mimeType($path);

            return response(Storage::get('diary/'. $image), 200, ['Content-Type' => $type]);
        }

        return response()->json(['Status Code' => '404 Not Found'], 404);
    }
}
