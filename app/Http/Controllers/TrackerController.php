<?php

namespace App\Http\Controllers;

use App\Diary;
use App\Http\Requests\TrackerAuthenticateRequest;
use App\Http\Requests\TrackerDiaryStoreRequest;
use App\Project;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class TrackerController extends Controller
{
    /**
     * Authenticate the users from the TimeCop tracker
     *
     * @param TrackerAuthenticateRequest|Request $request
     * @return string
     */
    public function authenticate(TrackerAuthenticateRequest $request)
    {
        $user = User::where('email', $request->email)->first();

        if (Hash::check($request->password, $user->password)) {

            return 'success';
        }

        return 'failed';
    }

    /**
     * Store a newly created tracker data in storage.
     *
     * @param TrackerDiaryStoreRequest|Request $request
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function store(Request $request)
    {
        $user = User::where('email', $request->email)->first();

        $project = Project::all()->where('title', $request->project)->first();

        $modified_request = array_add($request->all(), 'user_id', $user->id);

        $modified_request = array_add($modified_request, 'project_id', $project->id);

        $modified_request = array_except($modified_request, ['screenshot', 'webcam']);

        if (Hash::check($request->password, $user->password)) {

            Diary::create($modified_request);

            Storage::put('diary/' . $request->screenshotFileName, base64_decode($request->screenshot));

            if (!empty($request->webcam)) {

                Storage::put('diary/' . $request->webcamFileName, base64_decode($request->webcam));
            }

            return 'success';
        }

        return response()->json(['status' => 'Unauthorized access', 'code' => 403], 403);
    }

    /**
     * Provide projects to the Tracker
     *
     * @param TrackerAuthenticateRequest $request
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function projects(TrackerAuthenticateRequest $request)
    {
        $user = User::where('email', $request->email)->first();

        if (Hash::check($request->password, $user->password)) {

            $project = $user->project;
            $project = (object)$project->pluck('title')->toArray();

            return response()->json($project);
        }

        return 'error';
    }
}