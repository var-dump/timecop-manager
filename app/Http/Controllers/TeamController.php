<?php

namespace App\Http\Controllers;

use App\Http\Requests\TeamRequest;
use App\Team;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('team.view')->with(['teams' => Team::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('team.create')->with(['users' => User::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param TeamRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(TeamRequest $request)
    {
        Team::create($request->all());

        if (count($request->user)) {

            foreach ($request->user as $user) {
                User::where('id', $user)->update(['team_id' => Team::all()->last()->id]);
            }
        }

        return redirect()->back()->with('status', trans('Team has been created successfully.'))->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param Team $team
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function show(Team $team)
    {

        return view('team.single', compact('team'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Team $team
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function edit(Team $team)
    {

        return view('team.edit')->with(['team' => $team, 'users' => User::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Team $team
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(Request $request, Team $team)
    {
        $team->update($request->all());

        if (count($request->user)) {

            foreach ($request->user as $user) {
                User::where('id', $user)->update(['team_id' => $team->id]);
            }
        }

        return redirect()->back()->with('status', trans('Team has been updated successfully.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Team::destroy($id);

        if (User::all()->contains('team_id', $id)) {

            User::where('team_id', $id)->update(['team_id' => 0]);
        }

        return redirect()->back()->with('status', trans('Team has been deleted successfully.'));
    }
}
