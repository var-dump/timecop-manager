<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Diary extends Model
{
    protected $fillable = [
        'user_id',
        'second',
        'time',
        'project_id',
        'project',
        'workId',
        'screenshotFileName',
        'webcamFileName',
        'keyboard',
        'drags',
        'clicks',
        'status',
        'ip',
        'localTime',
        'city',
        'country',
        'mood'
    ];

    /**
     * One work diary belongs to only one user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function project()
    {
        return $this->belongsTo(Project::class);
    }
}
